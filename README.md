# README #

Bash script that simplyfied nmap with tcp and udp scans.

Originally by: https://gist.github.com/audrummer15/7c8c3dc54d5c21d588a7b1ba1b4ef66d

### List of script ###

* nmap-tcp-quick.sh
* nmap-tcp-full.sh
* nmap-udp-quick.sh

### Usage ###

* nmap-tcp-quick.sh <TARGET> <OUTPUT-FILENNAME>
* nmap-tcp-full.sh <TCP-QUICK-RESULTS.xml> <TARGET> <OUTPUT-FILENAME>
* nmap-udp-quick.sh <TARGET> <OUTPUT-FILENAME>

### Misc ###

For convenience you might wanna add this repo into bashrc script e.g: 

export PATH=/path/:"$:PATH"
. .bashrc